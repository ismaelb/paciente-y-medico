class Persona:
    def __init__(self, name, lastname, birth, dni):
        self._name = name
        self._lastname = lastname
        self._birth = birth
        self._dni = dni

    def setName(self, name):
        self._name = name
    def getName(self):
        return self._name
    
    def setLastname(self, lastname):
        self._lastname = lastname
    def getLastname(self):
        return self._lastname
    
    def setBirth(self, birth):
        self._birth = birth
    def getBirth(self):
        return self._birth
    
    def setDni(self, dni):
        self._dni = dni
    def getDni(self):
        return self._dni
    
    def __str__(self):
        return f"{self._name} {self._lastname}, nacido el {self._birth} cuyo DNI es {self._dni}"
    
persona1 = Persona("Ismael", "Bouterfas El Idrissi", "15 de octubre de 2005", "75919670M")

    
class Paciente(Persona):

    def __init__(self, name, lastname, birth, dni, historial_clinico):
        super().__init__(name, lastname, birth, dni)
        self._historial_clinico = historial_clinico

    def ver_historial_clinico(self):
        return self._historial_clinico
    
    def __str__(self):
        return f"El historial clinico del paciente {self._name} {self._lastname} es: {self._historial_clinico}"
    
    
class Medico(Persona):

    def __init__(self, name, lastname, birth, dni, especialidad, citas):
        super().__init__(name, lastname, birth, dni)
        self._especialidad = especialidad
        self._citas = citas

    def consultar_agenda(self):
        return self._citas
    
    def especialidad(self):
        return self._especialidad
    
    def __str__(self):
        return f"La especialidad del medico {self._name} {self._lastname} es {self._especialidad} y sus citas son: {self._citas}"
    

if __name__ == "__main__":
    paciente1 = Paciente("Ismael", "Bouterfas El Idrissi", "15 de octubre de 2005", "75919670M", "Ansiedad")
    print(paciente1)
    medico1 = Medico("Pedro", "Fernandez", "19 de marzo de 1985", "98763469L", "Oftalmología", "Mañana desde las 8 AM hasta las 1 AM")
    print(medico1) 






