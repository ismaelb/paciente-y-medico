import Paciente
import unittest

class MultiplyTestCase(unittest.TestCase):
    def test_ver_historial_clinico(self):
        paciente1 = Paciente.Paciente("Ismael", "Bouterfas El Idrissi", "15 de octubre de 2005", "75919670M", "Ansiedad")
        paciente1.ver_historial_clinico()
        self.assertEqual(paciente1._historial_clinico, "Ansiedad")


    def test_consultar_agenda(self):
        medico1 = Paciente.Medico("Pedro", "Fernandez", "19 de marzo de 1985", "98763469L", "Oftalmología", "Mañana desde las 8 AM hasta las 1 AM")
        medico1.consultar_agenda()
        self.assertEqual(medico1._citas, "Mañana desde las 8 AM hasta las 1 AM")


    def test_especialidad(self):
        medico1 = Paciente.Medico("Pedro", "Fernandez", "19 de marzo de 1985", "98763469L", "Oftalmología", "Mañana desde las 8 AM hasta las 1 AM")
        medico1.especialidad()
        self.assertEqual(medico1._especialidad, "Oftalmología")

if __name__=="__main__":
    unittest.main()